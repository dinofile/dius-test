'use strict';

const expect = require('chai').expect;
const { Catalogue, Product } = require('../src/catalogue')

describe('Catalogue', function() {

  let catalogue;

  beforeEach(function() {
    catalogue = new Catalogue();
  });

  it('should successfully add a product ', function() {
    let success = catalogue.add('ipd', 'Super iPad', 54999);
    expect(success).to.equal(true);
  });

  it('should successfully retrieve an added product', function() {
    catalogue.add('ipd', 'Super iPad', 54999);
    let product = catalogue.find('ipd');
    expect(product).to.be.an.instanceof(Product);
    expect(product.sku).to.equal('ipd');
    expect(product.name).to.equal('Super iPad');
    expect(product.price).to.equal(54999);
  });

  it('should fail to add a product with same SKU', function() {
    catalogue.add('ipd', 'Super iPad', 54999);
    let success = catalogue.add('ipd', 'Super iPad', 54999);
    expect(success).to.equal(false);
  });


});