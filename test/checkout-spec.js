'use strict';

const expect = require('chai').expect;
const { Checkout } = require('../src/checkout')
const { Catalogue } = require('../src/catalogue')
const { PricingRules, rules } = require('../src/pricing')

describe('Checkout', function() {

  let checkout;

  describe('No Pricing Rules nor scans', function() {

    it('should have total() method', function() {
      checkout = new Checkout();
      expect(checkout).to.have.property('total')
        .that.is.an('function');
    });

    it('should have scan() method', function() {
      checkout = new Checkout();
      expect(checkout).to.have.property('scan')
        .that.is.an('function');
    });
    
    it('should have zero total when nothing scanned', function() {
      checkout = new Checkout();
      expect(checkout.total()).to.equal('$0.00');
    });
  });  

  describe('No Pricing Rules but testing basic scans', function() {

    let catalogue;

    beforeEach(function() {

      // Add our inventory

      catalogue = new Catalogue();
      catalogue.add('ipd', 'Super iPad', 54999);
      
      // Create our Checkout
      checkout = new Checkout();
    });

    it('should return checkout items', function() {
      checkout.scan(catalogue.find('ipd'));
      const cart = checkout.getCart();
      expect(cart.length).to.equal(1);
    });

    it('should clear the checkout', function() {
      checkout.scan(catalogue.find('ipd'));
      checkout.clear();
      const cart = checkout.getCart();
      expect(cart.length).to.equal(0);
    });
  });  

  describe('With Pricing Rules', function() {

    let catalogue;
    let pricingRules;

    beforeEach(function() {

      // Add our inventory

      catalogue = new Catalogue();
      catalogue.add('ipd', 'Super iPad', 54999);
      catalogue.add('mbp', 'MacBook Pro', 139999);
      catalogue.add('atv', 'Apple TV', 10950);
      catalogue.add('vga', 'VGA Adapter', 3000);
      
      // Add pricing rules

      pricingRules = new PricingRules(catalogue);
      pricingRules.add(rules.buyXpayY('atv', 3, 2));
      pricingRules.add(rules.bulkDiscountForMoreThanX('ipd', 4, 49999));
      pricingRules.add(rules.bundleFreeWithEachUnit('mbp', catalogue.find('vga')));

      // Create our Checkout
      checkout = new Checkout(pricingRules);
    });

    it('should have zero total when nothing scanned', function() {
      expect(checkout.total()).to.equal('$0.00');
    });

    it('should have correct 3 for 2 deal', function() {
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('vga'));

      expect(checkout.total()).to.equal('$249.00');
    });

    it('should have correct 3 for 2 deal, double down', function() {
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('vga'));

      expect(checkout.total()).to.equal('$468.00');
    });

    it('should have correct 3 for 2 deal, extras', function() {
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('vga'));

      expect(checkout.total()).to.equal('$468.00');
    });

    it('should have correct Bulk Discount', function() {
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('ipd'));
      checkout.scan(catalogue.find('ipd'));
      checkout.scan(catalogue.find('atv'));
      checkout.scan(catalogue.find('ipd'));
      checkout.scan(catalogue.find('ipd'));
      checkout.scan(catalogue.find('ipd'));

      expect(checkout.total()).to.equal('$2718.95');
    });

    it('should not charge for free bundle item', function() {
      checkout.scan(catalogue.find('mbp'));
      checkout.scan(catalogue.find('vga'));
      checkout.scan(catalogue.find('ipd'));

      expect(checkout.total()).to.equal('$1949.98');
    });
    
    it('should charge for extra items non-bundled items', function() {
      checkout.scan(catalogue.find('mbp'));
      checkout.scan(catalogue.find('vga')); // This should be free
      checkout.scan(catalogue.find('ipd'));
      checkout.scan(catalogue.find('vga')); // This should be charged
      checkout.scan(catalogue.find('vga')); // This should be charged

      expect(checkout.total()).to.equal('$2009.98');
    });

    it('should add extra free bundled item', function() {
      checkout.scan(catalogue.find('mbp'));
      checkout.scan(catalogue.find('ipd'));

      expect(checkout.total()).to.equal('$1949.98');
    });
    
  });  
  
})