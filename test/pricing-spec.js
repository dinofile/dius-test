'use strict';

const expect = require('chai').expect;
const { PricingRules, rules } = require('../src/pricing')

describe('Pricing Rule Functions', function() {

  it('should return empty cart from calling buyXpayY rule', function() {
    const rule = rules.buyXpayY('atv', 3, 2);
    expect(rule).to.be.an('function');
    expect(rule([])).to.be.empty;
  });

  it('should return empty cart from calling bulkDiscountForMoreThanX rule', function() {
    const rule = rules.bulkDiscountForMoreThanX('ipd', 4, 49999);
    expect(rule).to.be.an('function');
    expect(rule([])).to.be.empty;
  });

  it('should return empty cart from calling bundleFreeWithEachUnit rule', function() {
    const rule = rules.bulkDiscountForMoreThanX('mbp', {});
    expect(rule).to.be.an('function');
    expect(rule([])).to.be.empty;
  });

});

describe('Pricing Rules', function() {

  let pricingRules;

  beforeEach(function() {
    pricingRules = new PricingRules();
  });

  it('should add rule successfully', function() {
    const rule = rules.buyXpayY('atv', 3, 2);
    pricingRules.add(rule);
    const allRules = pricingRules.getRules();
    expect(allRules.length).to.equal(1);
    expect(allRules[0]).to.equal(rule);
  });

  it('should remove rule successfully', function() {
    const rule = rules.buyXpayY('atv', 3, 2);
    pricingRules.add(rule);
    pricingRules.remove(rule);
    const allRules = pricingRules.getRules();
    expect(allRules).to.be.empty;
  });

});