## Prereqs

Requires Node v6+ for ES6 features. Used Node v7.5.0 in development.

## Install

```npm install```

## Run tests

```npm test```

## Run app

```npm start```