'use strict';

const { Checkout } = require('./checkout');
const { Catalogue } = require('./catalogue');
const { PricingRules, rules } = require('./pricing');

const catalogue = new Catalogue();
catalogue.add('ipd', 'Super iPad', 54999);
catalogue.add('mbp', 'MacBook Pro', 139999);
catalogue.add('atv', 'Apple TV', 10950);
catalogue.add('vga', 'VGA Adapter', 3000);

const pricingRules = new PricingRules(catalogue);
pricingRules.add(rules.buyXpayY('atv', 3, 2));
pricingRules.add(rules.bulkDiscountForMoreThanX('ipd', 4, 49999));
pricingRules.add(rules.bundleFreeWithEachUnit('mbp', catalogue.find('vga')));

const checkout = new Checkout(pricingRules);

// Scan some items

checkout.scan(catalogue.find('atv'));
checkout.scan(catalogue.find('atv'));
checkout.scan(catalogue.find('atv'));
checkout.scan(catalogue.find('vga'));

console.log(checkout.total(true));

checkout.clear();

checkout.scan(catalogue.find('atv'));
checkout.scan(catalogue.find('ipd'));
checkout.scan(catalogue.find('ipd'));
checkout.scan(catalogue.find('atv'));
checkout.scan(catalogue.find('ipd'));
checkout.scan(catalogue.find('ipd'));
checkout.scan(catalogue.find('ipd'));

console.log(checkout.total(true));

checkout.clear();

checkout.scan(catalogue.find('mbp'));
checkout.scan(catalogue.find('vga'));
checkout.scan(catalogue.find('ipd'));

console.log(checkout.total(true));
