'use strict';

class Product {
  constructor(sku = '', name = '', price = 0) {
    this.sku = sku;
    this.name = name;
    this.price = price;
  }
}

class Catalogue {
  constructor() {
    this.items = {};
  }

  add(sku = '', name = '', price = 0) {
    // Check if SKU is already in catalogue, if so fail, otherwise add
    // NOTE: Might an option to simply overwrite, but prefer to alert caller

    let found = this.items[sku];
    if (!found) {
      this.items[sku] = new Product(sku, name, price);
      return true;
    }
    return false;
  }

  find(sku) {
    return this.items[sku];
  }
}

module.exports = {
  Catalogue,
  Product
};

