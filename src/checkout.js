'use strict';

const formatCurrency = (amountInCents) => {
  const dollars = (amountInCents / 100).toFixed(2)
  return `$${dollars}`;
};

class Checkout {

  constructor(pricingRules) {
    this.cart = [];
    this.pricingRules = pricingRules || null;
  }

  clear() {
    this.cart = [];
  }

  getCart() {
    return this.cart;
  }
  
  scan(item) {
    // Group items with matching SKU together in single record
    if (!item) {
      throw new Error('Trying to scan a null item');
    }
    const sameItem = this.cart.find(it => it.sku === item.sku);
    if (sameItem) {
      sameItem.count++;
    } else {
      this.cart.push(Object.assign({}, item, {count: 1}));
    }
  }

  total(debug = false) {

    // First deep clone the cart, so we can call total() multiple times in between scans and not affect the original cart

    let cart = this.cart.map( item => Object.assign({}, item));

    // Run each pricing rule over cart

    this.pricingRules && this.pricingRules.getRules().forEach(rule => {
      // Have a catch here in case a rule decides to throw an error (tho not going to impl any actual throws in this demo)
      try {
        cart = rule(cart);
      } catch(e) {
        // TODO: Report to some logger
        console.log(e.message);
      }
    });

    if (debug) {
      console.log(cart);
    }

    return formatCurrency(
      cart.reduce((total, item) => {
        const count = item.freeCount ? item.count - item.freeCount : item.count;
        return total + (item.price * count);
      }, 0)
    );

  }

}

module.exports = {
  Checkout
};

