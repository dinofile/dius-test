'use strict';

// Pricing Rule Functions

// These functions must return a function with param signature of (cart),
// which when called should always return an updated cart array.

const buyXpayY = (sku, buyCount, payCount) => {
  // If you buy X number of an item, you pay for only Y items
  return cart => {
    return cart.map(item => {
      const buyGrouped = Math.floor(item.count / buyCount);
      if (item.sku !== sku || buyGrouped === 0) {
        return item;
      }
      const buyExtras = item.count % buyCount;
      return Object.assign({}, item, {count: ((buyGrouped * payCount) + buyExtras)});
    })
  };
}

const bulkDiscountForMoreThanX = (sku, thresholdCount, bulkPrice) => {
  return cart => {
    return cart.map(item => {
      if (item.sku !== sku || item.count < thresholdCount) {
        return item;
      }
      return Object.assign({}, item, {price: bulkPrice});
    });
  };
}


const bundleFreeWithEachUnit = (sku, bundledProduct) => {
  
  // TODO: perhaps more ideally the freeProduct could be a callback to fetch
  // the free product details in case it changes price for example since rule creation.
  
  return cart => {

    // Check if we have any target sku's in cart

    const targetItem = cart.find(item => item.sku === sku);
    if (!targetItem) {
      return cart; // None found, so return cart unmodified
    }
    
    // Find any items matching the bundled product
    const freeItem = cart.find(item => item.sku === bundledProduct.sku);
    const freeCount = (freeItem && freeItem.count) || 0;

    // We set count and introduce freeCount property
    const updateItem = Object.assign({}, bundledProduct, {
      count: Math.max(freeCount, targetItem.count),
      freeCount: targetItem.count
    });

    // Add our new bundled product object to cart
    const newCart = freeCount ? [
      ...(cart.filter(it => it.sku !== bundledProduct.sku)),
      updateItem
    ] : [
      ...cart,
      updateItem
    ];

    return newCart;
  }
}


class PricingRules {

  constructor() {
    this.rules = [];
  }

  getRules() {
    return this.rules;
  }

  add(rule) {
    this.rules.push(rule)
  }

  remove(rule) {
    this.rules = this.rules.filter( it => it !== rule );
  }
}

module.exports = {
  PricingRules,
  rules: {
    buyXpayY,
    bulkDiscountForMoreThanX,
    bundleFreeWithEachUnit
  }
}